package com.bookstore.bookstoreApp.entity.repository;

import com.bookstore.bookstoreApp.entity.Book;
import com.bookstore.bookstoreApp.entity.Customer;
import com.bookstore.bookstoreApp.entity.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends CrudRepository<Order,String> {

    Optional<Order> findById(String id);

    List<Order> findByCustomer(Customer customer);
    List<Order> findByBook(Book book);
}
