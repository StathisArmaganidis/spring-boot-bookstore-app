package com.bookstore.bookstoreApp.entity.repository;

import com.bookstore.bookstoreApp.entity.Customer;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CustomerRepository extends CrudRepository<Customer,String> {

    Optional<Customer> findById(String id);
}
