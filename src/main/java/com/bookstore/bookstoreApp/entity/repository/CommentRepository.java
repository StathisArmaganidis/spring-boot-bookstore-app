package com.bookstore.bookstoreApp.entity.repository;

import com.bookstore.bookstoreApp.entity.Book;
import com.bookstore.bookstoreApp.entity.Comment;
import com.bookstore.bookstoreApp.entity.Customer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CommentRepository extends CrudRepository<Comment,String> {

    Optional<Comment> findById(String id);

    List<Comment> findByCustomer(Customer customer);
    List<Comment> findByBook(Book book);
}
