package com.bookstore.bookstoreApp.entity.repository;

import com.bookstore.bookstoreApp.entity.Book;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface BookRepository  extends CrudRepository<Book, String> {
    Optional<Book> findById(String id);

}
