package com.bookstore.bookstoreApp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "books")
public class Book {

    @Id
    @Column(nullable = false, length = 45)
    private String id;

    @Column(nullable = false, length = 45)
    private String name;

    @Column
    private String description;

    @Column(nullable = false)
    private boolean available;

    @Column
    private int sold;

    @Column
    private int stock;

    public Book() {
    }

    public Book(String id, String name, String description, boolean available, int sold, int stock) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.available = available;
        this.sold = sold;
        this.stock = stock;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public int getSold() {
        return sold;
    }

    public void setSold(int sold) {
        this.sold = sold;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
