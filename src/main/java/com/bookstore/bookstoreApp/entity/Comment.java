package com.bookstore.bookstoreApp.entity;

import javax.persistence.*;

@Entity(name = "comments")
public class Comment {

    @Id
    @Column(nullable = false, length = 45)
    private String id;

    @OneToOne
    @JoinColumn(name = "cust_id")
    private Customer customer;

    @OneToOne
    @JoinColumn(name = "book_id")
    private Book book;

    @Column
    private String comment;

    public Comment() {
    }

    public Comment(String id, Customer customer, Book book, String comment) {
        this.id = id;
        this.customer = customer;
        this.book = book;
        this.comment = comment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
