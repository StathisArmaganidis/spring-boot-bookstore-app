package com.bookstore.bookstoreApp.entity.helper;

import org.springframework.beans.factory.annotation.Value;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;

@WebServlet("/TestDbConnection")
public class TestConnectionServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String user = "root";
        String pass = "root";
        String jdbcUrl = "jdbc:mysql://localhost:3306/bookstore:useSSL=false&serverTimezone=UTC";
        String driver = "com.mysql.jdbc.Driver";
        PrintWriter out = response.getWriter();
        out.println("Connecting to the database: " + jdbcUrl);
        try {



            Class.forName(driver);

            Connection myConn = DriverManager.getConnection(jdbcUrl, user, pass);

            out.println("Connection Successful");

            myConn.close();
        }catch(Exception e){}

    }
}
