package com.bookstore.bookstoreApp.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "orders")
public class Order {

    @Id
    @Column(nullable = false, length = 45)
    private String id;

    @OneToOne
    @JoinColumn(name = "book_id")
    private Book book;

    @OneToOne
    @JoinColumn(name = "cust_id")
    private Customer customer;

    @Column
    private Date date;

    public Order() {
    }

    public Order(String id, Book book, Customer customer, Date date) {
        this.id = id;
        this.book = book;
        this.customer = customer;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @DateTimeFormat(pattern = "dd-MMM-yyyy hh:mm")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
