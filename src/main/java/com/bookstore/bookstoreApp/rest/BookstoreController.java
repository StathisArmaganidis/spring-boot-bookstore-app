package com.bookstore.bookstoreApp.rest;

import com.bookstore.bookstoreApp.entity.Book;
import com.bookstore.bookstoreApp.entity.Comment;
import com.bookstore.bookstoreApp.entity.Customer;
import com.bookstore.bookstoreApp.entity.Order;
import com.bookstore.bookstoreApp.entity.repository.BookRepository;
import com.bookstore.bookstoreApp.entity.repository.CommentRepository;
import com.bookstore.bookstoreApp.entity.repository.CustomerRepository;
import com.bookstore.bookstoreApp.entity.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Controller
public class BookstoreController {

    @Autowired
    private BookRepository bookRepo;

    @Autowired
    private CommentRepository commentRepo;

    @Autowired
    private CustomerRepository customerRepo;

    @Autowired
    private OrderRepository orderRepo;

    public BookstoreController() {
    }

    /**
     * Shows home-page.html
     * @param model The data of all the books and customers
     * @return
     */
    @GetMapping(value = "/")
    public String homePage(Model model) {

        model.addAttribute("selectedBook", new Book());
        model.addAttribute("selectedCustomer", new Customer());
        model.addAttribute("bookOptions", bookRepo.findAll());
        model.addAttribute("customerOptions", customerRepo.findAll());

        return "home-page";
    }

    @GetMapping("/newCustomer")
    public String addNewCustomerTransition(Model model){
        model.addAttribute("newCustomer", new Customer());
        return "new-customer-page";
    }

    @GetMapping("/saveCustomer")
    public String saveNewCustomer(@ModelAttribute Customer customer, Model model){
        if(!customerRepo.existsById(customer.getId())) {
            customerRepo.save(customer);
            return "redirect:/";
        }else {
            model.addAttribute("type", "Customer");
            model.addAttribute("id", customer.getId());
            model.addAttribute("name", customer.getFirstName() + " " + customer.getLastName());
            return "error-page";
        }
    }

    @GetMapping(value = "/bookData")
    public String showBookData(@ModelAttribute Book book,Model model){

        model.addAttribute("name", bookRepo.findById(book.getId()).get().getName());
        model.addAttribute("book", book);
        model.addAttribute("orders", orderRepo.findByBook(book));
        model.addAttribute("comments", commentRepo.findByBook(book));
        return "book-page";
    }

    @GetMapping(value = "deleteComment", params = {"deleteComment"})
    public String deleteComment(final HttpServletRequest req){
        String id = req.getParameter("deleteComment");
        Comment temp = commentRepo.findById(id).get();

        commentRepo.deleteById(temp.getId());
        return "redirect:/bookData?id="+temp.getBook().getId();
    }

    @GetMapping(value = "/deleteBook")
    public String deleteBook(@ModelAttribute Book book, Model model){
        Optional<Book> tempData = bookRepo.findById(book.getId());
        if(tempData.isPresent()){
            model.addAttribute("tempData", tempData.get());
            model.addAttribute("type", "Book");
            commentRepo.findByBook(tempData.get()).forEach(comment -> { commentRepo.deleteById(comment.getId());});
            orderRepo.findByBook((tempData.get())).forEach(order -> { orderRepo.deleteById(order.getId());});
            bookRepo.deleteById(book.getId());
            return "delete-page";
        }
        return "redirect:/bookData";
    }

    @GetMapping(value = "/customerData")
    public String showCustomerData(@ModelAttribute Customer customer,Model model){

        model.addAttribute("name", customerRepo.findById(customer.getId()).get().getFirstName()
                +" "+
                customerRepo.findById(customer.getId()).get().getLastName());
        model.addAttribute("customer", customer);
        model.addAttribute("orders", orderRepo.findByCustomer(customer));
        model.addAttribute("comments", commentRepo.findByCustomer(customer));

        model.addAttribute("books", bookRepo.findAll());

        //model attributes for new comment
        model.addAttribute("newComment", new Comment());

        //model attributes for new order
        model.addAttribute("newOrder", new Order());
        return "customer-page";
    }

    @GetMapping(value = "/deleteCustomer")
    public String deleteCustomer(@ModelAttribute Customer customer, Model model){
        Optional<Customer> tempData = customerRepo.findById(customer.getId());
        if(tempData.isPresent()){
            model.addAttribute("tempData", tempData.get());
            model.addAttribute("type", "Customer");
            commentRepo.findByCustomer(tempData.get()).forEach(comment -> { commentRepo.deleteById(comment.getId());});
            orderRepo.findByCustomer((tempData.get())).forEach(order -> { orderRepo.deleteById(order.getId());});
            customerRepo.deleteById(customer.getId());
            return "delete-page";
        }
        return "redirect:/bookData";
    }

    @GetMapping(value = "/newComment")
    public String newComment(@ModelAttribute Comment comment, @ModelAttribute Customer customer){
        String tempId = idGenerator("comment",commentRepo);

        commentRepo.save(new Comment(tempId,
                customerRepo.findById(customer.getId()).get(),
                bookRepo.findById(comment.getBook().getId()).get(),comment.getComment()));
        return "redirect:/customerData?id="+customer.getId();
    }

    @GetMapping(value = "/newOrder")
    public String newOrder(@ModelAttribute Order order, @ModelAttribute Customer customer){
        String tempId = idGenerator("order", orderRepo);
        orderRepo.save(new Order(tempId,
                bookRepo.findById(order.getBook().getId()).get(),
                customerRepo.findById(customer.getId()).get(),new Date()));
        return "redirect:/customerData?id="+customer.getId();
    }

    @GetMapping("/newBook")
    public String addNewBookTransition(Model model){
        model.addAttribute("newBook", new Book());
        return "new-book-page";
    }

    @GetMapping("/saveBook")
    public String saveNewBook(@ModelAttribute Book book, Model model){
        book.setSold(0);
        if(!bookRepo.existsById(book.getId())) {
            bookRepo.save(book);
            return "redirect:/";
        }else {
            model.addAttribute("type", "Book");
            model.addAttribute("id", book.getId());
            model.addAttribute("name", book.getName());
            return "error-page";
        }
    }

    public String idGenerator(String comp, CrudRepository repo){
        int i = 0;
        String id;
        do{
            id = comp+i;
            //if the id doesn't exist, return it
            if(!repo.existsById(id)){
                break;
            }
            i++;
        }while(true);
        return id;
    }

    /**
     * Adds sample data to all the tables of the database.
     * @return The books currently in the database.
     */
    @GetMapping("/TestDbAddContent")
    public @ResponseBody Iterable<Book> saveSampleData() {

        //Initialize Books
        Book book1 = new Book("LeftHandOfGodStandard", "Left Hand of God", "The dark story of a young boy being pressed into a sect dedicated...", true, 4, 2);
        Book book2 = new Book("ThePrincePremiumEdition018", "The Prince", "Without a doubt The Prince is...", true, 10, 4);
        Book book3 = new Book("BloodsongPaperback", "Bloodsong", "The Sixth Order, bastions of the Faith of the Realm...", true, 3, 5);
        Book book4 = new Book("TalesOfTerrorAnthologyHardcover", "Tales of Terror", "A collections of some of the best works of H.P. Lovecraft...", false, 9, 2);

        bookRepo.save(book1);
        bookRepo.save(book2);
        bookRepo.save(book3);
        bookRepo.save(book4);

        //Initialize Customers
        Customer cust1 = new Customer("AI123456", "Stathis", "Armaganidis");
        Customer cust2 = new Customer("AI109873", "Paul", "Someone");
        Customer cust3 = new Customer("AI135126", "Stef", "Armaganidis");
        Customer cust4 = new Customer("AI142336", "Giannis", "Barakos");
        Customer cust5 = new Customer("AI000000", "Null", "Zeroson");

        customerRepo.save(cust1);
        customerRepo.save(cust2);
        customerRepo.save(cust3);
        customerRepo.save(cust4);
        customerRepo.save(cust5);

        //Initialize Comments
        commentRepo.save(new Comment("comment1", cust1,book1, "I really loved this book"));
        commentRepo.save(new Comment("comment2", cust1,book3, "I really loved this book!!"));
        commentRepo.save(new Comment("comment3", cust3,book4, "I really loved this book!!"));
        commentRepo.save(new Comment("comment4", cust2,book1, "I, too, really loved this book!"));
        commentRepo.save(new Comment("comment5", cust5,book2, "I really loved this book!!"));
        commentRepo.save(new Comment("comment6", cust4,book2, "I really loved this book!!!!"));

        //Initialize Orders
        orderRepo.save(new Order("order1", book1, cust1, new Date()));
        orderRepo.save(new Order("order2", book2, cust2, new Date()));
        orderRepo.save(new Order("order3", book3, cust1, new Date()));
        orderRepo.save(new Order("order4", book3, cust4, new Date()));
        orderRepo.save(new Order("order5", book3, cust3, new Date()));
        orderRepo.save(new Order("order6", book4, cust1, new Date()));
        orderRepo.save(new Order("order7", book2, cust5, new Date()));
//
        return bookRepo.findAll();
    }
}
