package com.bookstore.bookstoreApp;

import com.bookstore.bookstoreApp.entity.Book;
import com.bookstore.bookstoreApp.entity.repository.BookRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BookstoreAppApplication {

	public static void main(String[] args) {

		SpringApplication.run(BookstoreAppApplication.class, args);
	}

}
